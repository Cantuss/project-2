grammar Expr;
program: statement + EOF ;

statement:                                               
    '{' statement (statement)* '}'                          #blockOfStatements
    | SEMI                                                  # emptyCommand     
    | type_=primitiveType names=IDENTIFIER ( COMMA additional_names += IDENTIFIER)* SEMI    # declaration 
    | expr SEMI                                             # expressionEvaluation
    | READ IDENTIFIER ( COMMA IDENTIFIER)* SEMI             # readValue
    | WRITE expr ( COMMA loadVariables += expr)* SEMI                        # writeValues         
    | IF '(' expr ')' pos= statement (ELSE neg = statement)?   # conditionalStatement           
    | WHILE '(' expr ')' content = statement                          # whileCycle
    | FOR '(' expr SEMI expr SEMI expr ')' statement   # forCycle
    ;


expr: IDENTIFIER                            # identifier            
    | ('true'|'false')                      # bool          
    | '(' expr ')'                          # parens        
    | INT                                   # int           
    | FLOAT                                 # float         
    | STRING                                # string        
    //expressions
    | prefix=SUB expr                       # unaryMinus
    | prefix=NEG expr                       # logicNot
    | left=expr op=(MUL|DIV|MOD) right=expr            # mulDivMod
    | left=expr op=(ADD|SUB|CON) right=expr            # addSubCon
    | left=expr op=(LES|GRE) right=expr                # relational
    | left=expr op=(EQ|NEQ) right=expr                 # comparison
    | left=expr AND right=expr                         # logicalAnd
    | left=expr OR right=expr                          # logicalOr
    | <assoc=right> left=IDENTIFIER '=' right=expr     # assignment
    | <assoc=right> left=expr QUESTION_MARK expression1=expr COLON expression2=expr               #ternaryOperator
    ;

primitiveType:
    type=INT_KEYWORD
    | type=FLOAT_KEYWORD
    | type=STRING_KEYWORD
    | type=BOOL_KEYWORD
    ;


INT_KEYWORD : 'int';
FLOAT_KEYWORD : 'float';
STRING_KEYWORD : 'string';
BOOL_KEYWORD : 'bool';

SEMI:               ';';
COMMA:              ',';
COLON:              ':';
QUESTION_MARK:      '?';

CON : '.' ;
MUL : '*' ;
DIV : '/' ;
MOD : '%' ;
ADD : '+' ;
SUB : '-' ;
LES : '<' ;
GRE : '>' ;
NEG : '!' ;
EQ  : '==' ;
NEQ : '!=' ;
AND : '&&' ;
OR : '||' ;

READ : 'read' ;
WRITE : 'write' ;
IF : 'if' ;
ELSE : 'else' ;
WHILE : 'while' ;
FOR : 'for' ;

IDENTIFIER : [a-zA-Z] ([a-zA-Z0-9]*)? ;

INT : [0-9]+ ;
FLOAT : [0-9]+'.'[0-9]+ ;
BOOL : ('true'|'false') ;
STRING : '"' (~["\\\r\n] | ESCAPE_CHAR)* '"'; //escape sequence is optional

fragment ESCAPE_CHAR
 : '\\' [0btnfr"'\\]
 ;

WS : [ \t\r\n]+ -> skip ; // toss out whitespace
COMMENT: '/*' .*? '*/' -> skip ;
LINE_COMMENT: '//' ~[\r\n]* -> skip ;