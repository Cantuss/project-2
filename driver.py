import sys
import ast
from VirtualMachine import VirtualMachine
from antlr4 import *
from ExprLexer import ExprLexer
from ExprParser import ExprParser
from ExprListener import ExprListener
from ExprVisitor import ExprVisitor
from antlr4.error.ErrorListener import ErrorListener

typeCheckingErrors = 0
numberOfValues = 0
variableNames = []
variablesDict = {}
doNotPop = []
uniqueJumpLabel = -1

class EvalVisitor (ExprVisitor):
    def __init__(self):
        super(EvalVisitor, self).__init__()
        self.res={}
        variablesDict={}

    def visitAddSubCon(self, context):
        left = self.visit(context.left)
        right = self.visit(context.right)
        operator = context.op.text;
        if operator == "+":
            if (left.split()[1] == "I" and right.split()[1] == "F") or (left.split()[1] == "F" and right.split()[1] == "I") or ("itof" in right):
                return str(left) + "itof\n" + str(right)+ "add\n"
            return str(left) + str(right) + "add\n"
        elif operator == "-":
            if (left.split()[1] == "I" and right.split()[1] == "F") or (left.split()[1] == "F" and right.split()[1] == "I") or ("itof" in right):
                return str(left) + "itof\n" + str(right)+ "sub\n"
            return str(left) + str(right) + "sub\n"
        elif operator == ".":
            return str(left) + str(right) + "concat\n"

    def visitParens(self, context):
        return self.visit(context.expr())

    def visitProgram(self, context):
        global numberOfValues
        programCode = ""
        #print("context expr:"+str(context.statement()))
        for statement in context.statement():
            code = self.visit(statement)
            #print(code)
            if code is not None:
                programCode += code
                #programCode += "print "+str(numberOfValues)+"\n"
                numberOfValues = 0
                #programCode += "pop\n"
        return programCode

    def visitReadValue(self, context):
        #print(context.IDENTIFIER())
        code = ""
        content = context.IDENTIFIER()
        for variable in content:
            #print(variable)
            if variablesDict[str(variable)] is int:
                code += "read I\nsave "+str(variable)+"\n"
            elif variablesDict[str(variable)] is str:
                code += "read S\nsave "+str(variable)+"\n"
            elif variablesDict[str(variable)] is float:
                code += "read F\nsave "+str(variable)+"\n"
            elif variablesDict[str(variable)] is bool:
                code += "read B\nsave "+str(variable)+"\n"
        #if variablesDict[str(context.IDENTIFIER()[0])] == "int":
        #    return "read I \n"
        #elif variablesDict[str(context.IDENTIFIER()[0])] == "string":
        #    return "read S \n"
        #elif variablesDict[str(context.IDENTIFIER()[0])] == "float":
        #    return "read F \n"
        #elif variablesDict[str(context.IDENTIFIER()[0])] == "bool":
        #    return "read B \n"
        return code
    
    def visitWriteValues(self, context):
        global variableNames
        code = ""
        global numberOfValues
        numberOfValues = 0
        #sprint(self.visit(context.expr()[0]))
        exprTest = self.visit(context.expr()[0]) #push S "s(Abcd): "
        exprSplit = exprTest.split()[2] #"s(Abcd)"
        #print(exprSplit)
        if "(" in exprSplit:
            exprVariable = exprSplit.replace('"',"").split("(")[0]
            #print(exprVariable)
            #if exprVariable in variablesDict:
                #code += "save "+exprVariable+"\nload "+exprVariable+"\npop\n"
        for expr in context.expr():
            exprVisit = self.visit(expr)
            if exprVisit != None:
                #print("test"+self.visit(expr))
                
                #print(context.loadVariables)
                #for variable in context.loadVariables:
                #    if variable.getText() in variablesDict:
                #        code+="save "+str(variable.getText())+"\nload "+str(variable.getText())+"\npop\n"


                code += self.visit(expr)

            numberOfValues+=1
        #print("\n")
        #print(numberOfValues)
        return code + "print "+str(numberOfValues)+"\n"
        #return code #+ "\n"
        
    def visitComparison(self, context):
        operator = context.op.text
        left=self.visit(context.left)
        right=self.visit(context.right)

        if operator == "==":
            return left+right+"eq\n"
        else:
            return left+right+"eq\nnot\n"
    
    def visitRelational(self, context):
        code = ""
        operator = context.op.text
        left=self.visit(context.left)
        right=self.visit(context.right)

        if left.split()[1] == "I" and right.split()[1] == "F":
            code += left + "itof\n"
        else:
            code += left
        
        if operator == "<":
            code += right+"lt "
        else:
            code += right+"gt "
        
        if left.split()[1] == "I" and right.split()[1] == "F" or right.split()[1] == "I" and left.split()[1] == "F":
            code += "F\n"
        else:
            code += left.split()[1]+"\n"
        #code += "pop\n"
        return code

    def visitLogicNot(self, context):
        return self.visit(context.expr())+"not\n"

    def visitLogicalAnd(self, context):
        left=self.visit(context.left)
        right=self.visit(context.right)
        return left+right+"and\n"
        
    def visitLogicalOr(self, context):
        left=self.visit(context.left)
        right=self.visit(context.right)
        return left+right+"or\n"




    def visitMulDivMod(self, context):
        left = self.visit(context.left)
        right = self.visit(context.right)
        operator = context.op.text;

        code = ""
        #print(left.split()[1])
        #print(right.split()[1])
        #print("\n")
        if operator == "*":
            if (left.split()[1] == "I" and right.split()[1] == "F") or (left.split()[1] == "F" and right.split()[1] == "I") or ("itof" in right):
                return str(left) + "itof\n" + str(right)+ "mul\n"
            return code + str(left) + str(right) + "mul\n"
        elif operator == "/":
            return code + str(left) + str(right) + "div\n"
        elif operator == "%":
            return code + str(left) + str(right) + "mod\n"

    def visitExpressionEvaluation(self, context):
        #print(context.expr())
        return self.visit(context.expr())
        #for expr in context.expr: 
        #    return self.visit(expr)

    def visitAssignment(self, context):
        l = context.left
        l_type = l.type
        l_name = l.text
        #r = self.visit(context.right)
        #print("save "+l_name)
        #return "save "+l_name
        #print(variablesDict[l_name])
        #print(self.visit(context.right).split()[1])
        #print("\n")
        #print(self.visit(context.right))
        code = ""

        #print(self.visit(context.expr()))
        #print(context.right.getText())
        if l_name in variablesDict:
            code += "save "+l_name+"\nload "+l_name+"\n"+"pop"+"\n"
        if variablesDict[l_name] is float and self.visit(context.right).split()[1] == "I":
            return self.visit(context.right) + "itof\n" + code
        #if ("save" not in self.visit(context.right)) or ("pop" not in self.visit(context.right)):
        if "=" in context.right.getText(): #Tato cast tady je z toho duvodu, ze u multiple assignments chci dat pop az na konci, ne pro vsechny variables
            multipleAssigmentVariables = context.right.getText().split("=")
            for variable in multipleAssigmentVariables:
                if variable not in doNotPop:
                    doNotPop.append(variable)
        if l_name in doNotPop:
            return self.visit(context.right)   +"save "+l_name+"\nload "+l_name+"\n"
        return self.visit(context.right)   +"save "+l_name+"\nload "+l_name+"\n"+"pop\n" #spatny pop
        #else:
        #return self.visit(context.right)

    def visitDeclaration(self, context):
        global variableNames
        code = ""
        names_list = context.IDENTIFIER()
        names_list

        if len(names_list)>1:
            print(len(names_list))
            for variable_name in names_list:
                code+="push I 0\n"
                #code+="save "+str(variable_name)+"\nload "+str(variable_name)+"\n"
                code += "save "+str(variable_name)+"\n"
            #code += "pop\n"
        else:
            for id in context.IDENTIFIER():
                #code+="save "+str(id)+"\n"
                #print(id.getText())
                if id.getText() not in variableNames:
                    variableNames.append(id.getText())
                    #print(variableNames)
                if variablesDict[str(context.IDENTIFIER()[0])] is str:
                    code += 'push S ""\n'
                elif variablesDict[str(context.IDENTIFIER()[0])] is float:
                    code += 'push F 0.0\n'
                elif variablesDict[str(context.IDENTIFIER()[0])] is int:
                    code += 'push I 0\n'
                elif variablesDict[str(context.IDENTIFIER()[0])] is bool:
                    code += 'push B true\n'
                code += "save "+str(context.IDENTIFIER()[0])+"\n"
        
        #return "push TEST "+str(context.IDENTIFIER()[0]) + "\n"
        #return code + ("save "+ str(context.IDENTIFIER()[0]) + "\nload " + str(context.IDENTIFIER()[0]) + "\npop\n")
        return code
            
    def visitConditionalStatement(self, context):
        #print(self.visit(context.expr()))
        global uniqueJumpLabel
        uniqueJumpLabel+=1
        #print(self.visit(context.statement()[1]))
        #print(context.pos.getText())
        if context.neg != None:
            uniqueJumpLabel+=1
            return self.visit(context.expr()) + "fjmp "+str(uniqueJumpLabel-1)+"\n"+self.visit(context.neg)+"jmp "+str(uniqueJumpLabel)+"\n"+"label "+str(uniqueJumpLabel-1)+"\n"+self.visit(context.pos)+"label "+str(uniqueJumpLabel)+"\n"
        return self.visit(context.expr()) + "fjmp "+str(uniqueJumpLabel)+"\n"+self.visit(context.pos)+"jmp "+ str(uniqueJumpLabel+1) +"\n"+"label "+str(uniqueJumpLabel)+"\n"+"label "+str(uniqueJumpLabel+1)+"\n"

    def visitWhileCycle(self, context):
        global uniqueJumpLabel
        left = self.visit(context.expr())
        content = self.visit(context.content)
        uniqueJumpLabel += 1
        code= "label "+str(uniqueJumpLabel+1)+"\n"+left + "fjmp "+str(uniqueJumpLabel+2) + "\n" + content + "jmp "+str(uniqueJumpLabel+1) + "\n" + "label "+ str(uniqueJumpLabel+2)+"\n" #"jmp " + str(uniqueJumpLabel) +"\n"+ "label "+str(uniqueJumpLabel-1) +"\n"+ "label " + str(uniqueJumpLabel) +"\n"
        uniqueJumpLabel +=1
        return code

    def visitBlockOfStatements(self, context):
        #print("nic")
        #print(context.statement())
        code = ""
        for content in context.statement():
            #print(self.visit(content))
            code += self.visit(content)

        return code

    def visitIdentifier(self, context):
        #if context.getText() not in variablesDict:
        #    print(context.getText())
        #    return "save\nload\npop\n"
        return "load "+ str(context.getText()) + "\n" #novy s

    def visitUnaryMinus(self, context):
        return self.visit(context.expr())+"uminus\n"

    def visitString(self, ctx):
        global numberOfValues
        #numberOfValues+=1
        #return str(ctx.getText())
        return "push S "+(str(ctx.getText())) + "\n"
    def visitInt(self, ctx):
        global numberOfValues
        #numberOfValues+=1
        #return int(ctx.getText())
        return "push I "+str(int(ctx.getText()))+ "\n"
    def visitBool(self, ctx):
        global numberOfValues
        #numberOfValues+=1
        #return bool(self,ctx.getText())
#        print("test:"+str(bool(ctx.getText())))
        return "push B "+str((ctx.getText()))+ "\n"
        #if(str(ctx.getText())=="true")
    def visitFloat(self, ctx):
        global numberOfValues
        #numberOfValues+=1
        #return float(ctx.getText())
        return "push F "+str(float(ctx.getText()))+ "\n"
    
class TypeCheckingVisitor (ExprVisitor):
    def __init__(self):
        super(TypeCheckingVisitor, self).__init__()
        self.res={}
        global variablesDict
        variablesDict = {}
        print("--------------------------------------------------------")

    def visitIdentifier(self, context):
        #print(context.getText())
        variable_name = context.getText()
        #if variable_name not in variablesDict:
        #    if len(variablesDict) == 0:
        #        variablesDict[variable_name] = None
        #    variablesDict[variable_name] = variablesDict[list(variablesDict.keys())[-1]]
    def visitMulDivMod(self, context):
        global typeCheckingErrors
        left = self.visit(context.left)
        right = self.visit(context.right)
        operator = context.op.text;
        if operator == "%":
            if type(left) is float or type(right) is float:
                print("TYPE CHECKING ERROR: mod used with float")
                print("in ["+context.getText()+"]\n")
                typeCheckingErrors+=1
    
    
    def visitAddSubCon(self,context):
        global typeCheckingErrors
        left = self.visit(context.left)
        right = self.visit(context.right)
        #print(left)
        #print(right)
        operator = context.op.text;
        #print(operator)
        if operator == ".":
            #if variablesDict[left] != "string" and variablesDict[right] != "string":
            #    print("TYPE CHECKING ERROR: '.' is only for strings")
            if type(left) is not str or type(right) is not str:
                print("TYPE CHECKING ERROR: '.' is only for strings")
                print("in ["+context.getText()+"]\n")
                typeCheckingErrors+=1
        elif operator == "+":
            if type(left) is str or type(right) is str:
                print("TYPE CHECKING ERROR: '+' wont work with strings")
                print("in ["+context.getText()+"]\n")
                typeCheckingErrors+=1

    def visitDeclaration(self, context):
        global typeCheckingErrors
        #print(context.IDENTIFIER(0).getText())
        variable_name = context.names.text
        additional_variable_names = context.additional_names
        #print(len(additional_variable_names))
        if variable_name in variablesDict.keys():
            print("TYPE CHECKING ERROR: variable " + variable_name + " already declared")
            print("in ["+context.getText()+"]\n")
            typeCheckingErrors+=1

        for additional_variable_name in additional_variable_names:
            if additional_variable_name in variablesDict.keys():
                print("TYPE CHECKING ERROR: variable " + additional_variable_name + " already declared")
                print("in ["+context.getText()+"]\n")
                typeCheckingErrors+=1
        
        variable_type = context.type_.getText()
        variablesDict[variable_name] = variable_type

        if len(additional_variable_names) != 0:
            #print("additional: "+str(additional_variable_names))
            for additional_variable_name in additional_variable_names:
                additional_variable_name = additional_variable_name.text
                variablesDict[additional_variable_name] = variable_type
                #print("adding additional variable "+str(additional_variable_name)+" of type "+ variable_type)

        #additional_variable_names = context.additional_names
        #print(variable_name)
        #print(additional_variable_names)
        
        #print(variable_name)
        #print(variable_type)
        
        #print(variablesDict)

    def visitAssignment(self, context):
        global typeCheckingErrors
        #print("test")
        #l = self.visit(context.left)
        l = context.left
        l_type = l.type
        l_name = l.text
        #print(context.right.getText())
        #r = context.right.getText()
        r = self.visit(context.right)
        if r == None:
           r = context.right.getText()
           if "=" in r:
            r_multiple = r[r.rfind('=')+1:len(r)]
            #print(l_name)
            #print("r_multiple: "+ r_multiple)
            r = ast.literal_eval(r_multiple)

        #r = context.right.getText()
        r_type = str(type(r).__name__)

        if r == "true" or r == "false":
            r_type = "bool"

        if r_type == "str":
            r_type = "string"
        #print(l_name)
        #print(variablesDict)
        if l_name in variablesDict:
            variableDictionaryType = variablesDict[l_name]
            print("\nvariable " +  l_name  + " from the dictionary has type: " + variableDictionaryType)
            print("-> trying to assign value" +" (" + str(r) + ") " + " of type: " + r_type)
            if r_type != variableDictionaryType:
                if (r_type == "float") and (variableDictionaryType is int):
                    print("TYPE CHECKING ERROR : Cannot convert from float to int")
                    print("in ["+context.getText()+"]\n")
                    typeCheckingErrors+=1
        else:
            print("TYPE CHECKING ERROR: variable: "+l_name+" was not declared")
            print("in ["+context.getText()+"]\n")
            typeCheckingErrors+=1
            #print(variablesDict)
            
        #print(str(l_text) + " " +  str(l_type) + " = ")
        #print(str(r) + " : " + str(type(r)))
        #r = self.visit(context.getText())
        #return r

    def visitTernaryOperator(self, context):
        global typeCheckingErrors
        #print(context.left.getText())
        left_test = self.visit(context.left)
        #print(left_test)
        expression1 = context.expression1.getText()
        expression2 = context.expression2.getText()
        expression1_type = type(ast.literal_eval(expression1))
        expression2_type = type(ast.literal_eval(expression2))
        #print(expression1_type)
        #print(expression2_type)
        #print(self.visit(context.left))
        if left_test != bool:
            print("TYPE CHECKING ERROR : First expression in ternary operator must be bool")
            print("in ["+context.getText()+"]\n")
            typeCheckingErrors+=1
            
        if expression1_type != expression2_type:
            if (expression1_type not in [int,float]) or (expression2_type not in [int,float]):
                print("TYPE CHECKING ERROR: Types of expressions on the right side of the ternary operator are not the same.")
                print("in ["+context.getText()+"]\n")
                typeCheckingErrors+=1

    def visitComparison(self, ctx):
        return bool

    def visitString(self, ctx):
        return str(ctx.getText())
    def visitInt(self, ctx):
        return int(ctx.getText())
    def visitBoolean(self, ctx):
        return bool(self,ctx.getText())
    def visitFloat(self, ctx):
        return float(ctx.getText())
    #def visitNull(self, ctx):
    #    return None

class MyTypeCheckingVisitor (ExprVisitor):
    def __init__(self):
        super(MyTypeCheckingVisitor, self).__init__()
        self.res={}
        global variablesDict
        variablesDict = {}
        print("--------------------------------------------------------")

    def visitString(self, ctx):
        #return str(ctx.getText())
        return str
    def visitInt(self, ctx):
        #return int(ctx.getText())
        return int
    def visitBoolean(self, ctx):
        #if ctx.getText() == "true":
        #    return True
        #else:
        #    return False
        return bool
    def visitFloat(self, ctx):
        #return float(ctx.getText())
        return float

    def visitIdentifier(self,ctx):
        #print("gjgjfgfj: "+str(ctx.IDENTIFIER()))
        if ctx.IDENTIFIER() in variablesDict:
            return variablesDict[ctx.IDENTIFIER()]

    def visitMulDivMod(self, context):
        global typeCheckingErrors
        left = self.visit(context.left)
        right = self.visit(context.right)
        operator = context.op.text;
        if operator == "%":
            if left is float or right is float:
                print(str(context.start.line)+":"+str(context.start.column)+": "+"TYPE CHECKING ERROR: mod used with float")
                #print("in ["+context.getText()+"]\n")
                typeCheckingErrors+=1
            else:
                return int
    
    def visitAddSubCon(self,context):
        global typeCheckingErrors
        left = self.visit(context.left)
        right = self.visit(context.right)
        operator = context.op.text;
        if operator == ".":
            if left is not str or right is not str:
                print(str(context.start.line)+":"+str(context.start.column)+": "+"TYPE CHECKING ERROR: '.' is only for strings")
                #print("in ["+context.getText()+"]\n")
                typeCheckingErrors+=1
            else:
                return str
        elif operator == "+":
            if left is str or right is str:
                print(str(context.start.line)+":"+str(context.start.column)+": "+"TYPE CHECKING ERROR: '+' wont work with strings")
                #print("in ["+context.getText()+"]\n")
                typeCheckingErrors+=1
            else:
                return str

    def visitDeclaration(self, context):
        global typeCheckingErrors
        type = self.visit(context.primitiveType())
        for ident in context.IDENTIFIER():
            #print("id " + str(ident))
            if ident in variablesDict.keys():
                print(str(context.start.line)+":"+str(context.start.column)+": "+"TYPE CHECKING ERROR: variable " + variable_name + " already declared")
                #print("in ["+context.getText()+"]\n")
                typeCheckingErrors+=1
            else:
                variablesDict[str(ident)] = type
                #print(variablesDict)
    
    def visitAssignment(self, context):
        left = context.IDENTIFIER()
        if str(left) not in variablesDict:
            print(str(context.start.line)+":"+str(context.start.column)+": "+"TYPE CHECKING ERROR: missing declaration of variable: " + str(left))
        else:
            if variablesDict[str(left)] != self.visit(context.right):
                if variablesDict[str(left)] != float and self.visit(context.right) != int:
                    print(str(context.start.line)+":"+str(context.start.column)+": "+"TYPE CHECKING ERROR: trying to assign data of type " + str(self.visit(context.right)) + " a variable of type " + str(variablesDict[str(left)]))
    
    def visitTernaryOperator(self, context):
        global typeCheckingErrors
        left = self.visit(context.left)
        pos = self.visit(context.expression1)
        neg = self.visit(context.expression2)

        if left != bool:
            print(str(context.start.line)+":"+str(context.start.column)+": "+"TYPE CHECKING ERROR: lest side of the ternary operator must be a bool")
            #print("in ["+context.getText()+"]\n")
            typeCheckingErrors+=1

        #print(left)
        #print(pos)
    
    def visitPrimitiveType(self, context):
        if context.type_.text == "int":
            return int
        elif context.type_.text == "float":
            return float
        elif context.type_.text == "bool":
            return bool
        elif context.type_.text == "string":
            return str
        

class MyErrorListener( ErrorListener ):
    def syntaxError(self, recognizer, offendingSymbol, line, column, msg, e):
        print (str(line) + ":" + str(column) + ": syntax ERROR, " + str(msg))
        print ("Terminating Translation")
        print("------------------------------------------------------")
        #sys.exit()

    def reportAmbiguity(self, recognizer, dfa, startIndex, stopIndex, exact, ambigAlts, configs):
        print ("Ambiguity ERROR, " + str(configs))
        #sys.exit()

    def reportAttemptingFullContext(self, recognizer, dfa, startIndex, stopIndex, conflictingAlts, configs):
        print ("Attempting full context ERROR, " + str(configs))
        #sys.exit()

    def reportContextSensitivity(self, recognizer, dfa, startIndex, stopIndex, prediction, configs):
        print ("Context ERROR, " + str(configs))
        #sys.exit()

def main(argv):
    global typeCheckingErrors
    if len(sys.argv) > 1:
        input = FileStream(sys.argv[1])
    else:
        #input = InputStream(sys.stdin.readline())
        input = FileStream("testCycle.txt") #test, testComparison, testCycle

    lexer = ExprLexer(input)
    lexer.addErrorListener(MyErrorListener())
    tokens = CommonTokenStream(lexer)
    parser = ExprParser(tokens)
    parser.addErrorListener(MyErrorListener())
    print("\n------------------------------------------------------")
    tree = parser.program()
    print(tree.toStringTree(recog=parser))

    typeCheckingVisitor = MyTypeCheckingVisitor()
    typeCheckingVisitor.visit(tree)
    if typeCheckingErrors != 0:
        print(str(typeCheckingErrors) + " type checking errors detected. Stopping the computation.")
        exit()
    else:
        print("\nNo type checking errors found.")
    
    evalVisitor = EvalVisitor()
    output = evalVisitor.visit(tree)
    print(output)
    f = open("output.txt","w")
    f.write(output)
    f.close()

    virtualMachine = VirtualMachine(output)
    virtualMachine.run(0)

    #listener = ExprListener()
    #walker = ParseTreeWalker()
    #result = walker.walk(listener, tree)
    #print ("Results: " + str(result))

if __name__ == '__main__':
    main(sys.argv)