# Generated from Expr.g4 by ANTLR 4.13.0
# encoding: utf-8
from antlr4 import *
from io import StringIO
import sys
if sys.version_info[1] > 5:
	from typing import TextIO
else:
	from typing.io import TextIO

def serializedATN():
    return [
        4,1,42,143,2,0,7,0,2,1,7,1,2,2,7,2,2,3,7,3,1,0,4,0,10,8,0,11,0,12,
        0,11,1,0,1,0,1,1,1,1,1,1,5,1,19,8,1,10,1,12,1,22,9,1,1,1,1,1,1,1,
        1,1,1,1,1,1,1,1,5,1,31,8,1,10,1,12,1,34,9,1,1,1,1,1,1,1,1,1,1,1,
        1,1,1,1,1,1,1,1,5,1,45,8,1,10,1,12,1,48,9,1,1,1,1,1,1,1,1,1,1,1,
        5,1,55,8,1,10,1,12,1,58,9,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
        3,1,69,8,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,
        1,1,1,1,1,3,1,87,8,1,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,
        1,2,1,2,1,2,1,2,1,2,1,2,3,2,106,8,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,
        1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,1,2,
        1,2,5,2,132,8,2,10,2,12,2,135,9,2,1,3,1,3,1,3,1,3,3,3,141,8,3,1,
        3,0,1,4,4,0,2,4,6,0,5,1,0,5,6,1,0,17,19,2,0,16,16,20,21,1,0,22,23,
        1,0,25,26,170,0,9,1,0,0,0,2,86,1,0,0,0,4,105,1,0,0,0,6,140,1,0,0,
        0,8,10,3,2,1,0,9,8,1,0,0,0,10,11,1,0,0,0,11,9,1,0,0,0,11,12,1,0,
        0,0,12,13,1,0,0,0,13,14,5,0,0,1,14,1,1,0,0,0,15,16,5,1,0,0,16,20,
        3,2,1,0,17,19,3,2,1,0,18,17,1,0,0,0,19,22,1,0,0,0,20,18,1,0,0,0,
        20,21,1,0,0,0,21,23,1,0,0,0,22,20,1,0,0,0,23,24,5,2,0,0,24,87,1,
        0,0,0,25,87,5,12,0,0,26,27,3,6,3,0,27,32,5,35,0,0,28,29,5,13,0,0,
        29,31,5,35,0,0,30,28,1,0,0,0,31,34,1,0,0,0,32,30,1,0,0,0,32,33,1,
        0,0,0,33,35,1,0,0,0,34,32,1,0,0,0,35,36,5,12,0,0,36,87,1,0,0,0,37,
        38,3,4,2,0,38,39,5,12,0,0,39,87,1,0,0,0,40,41,5,29,0,0,41,46,5,35,
        0,0,42,43,5,13,0,0,43,45,5,35,0,0,44,42,1,0,0,0,45,48,1,0,0,0,46,
        44,1,0,0,0,46,47,1,0,0,0,47,49,1,0,0,0,48,46,1,0,0,0,49,87,5,12,
        0,0,50,51,5,30,0,0,51,56,3,4,2,0,52,53,5,13,0,0,53,55,3,4,2,0,54,
        52,1,0,0,0,55,58,1,0,0,0,56,54,1,0,0,0,56,57,1,0,0,0,57,59,1,0,0,
        0,58,56,1,0,0,0,59,60,5,12,0,0,60,87,1,0,0,0,61,62,5,31,0,0,62,63,
        5,3,0,0,63,64,3,4,2,0,64,65,5,4,0,0,65,68,3,2,1,0,66,67,5,32,0,0,
        67,69,3,2,1,0,68,66,1,0,0,0,68,69,1,0,0,0,69,87,1,0,0,0,70,71,5,
        33,0,0,71,72,5,3,0,0,72,73,3,4,2,0,73,74,5,4,0,0,74,75,3,2,1,0,75,
        87,1,0,0,0,76,77,5,34,0,0,77,78,5,3,0,0,78,79,3,4,2,0,79,80,5,12,
        0,0,80,81,3,4,2,0,81,82,5,12,0,0,82,83,3,4,2,0,83,84,5,4,0,0,84,
        85,3,2,1,0,85,87,1,0,0,0,86,15,1,0,0,0,86,25,1,0,0,0,86,26,1,0,0,
        0,86,37,1,0,0,0,86,40,1,0,0,0,86,50,1,0,0,0,86,61,1,0,0,0,86,70,
        1,0,0,0,86,76,1,0,0,0,87,3,1,0,0,0,88,89,6,2,-1,0,89,106,5,35,0,
        0,90,106,7,0,0,0,91,92,5,3,0,0,92,93,3,4,2,0,93,94,5,4,0,0,94,106,
        1,0,0,0,95,106,5,36,0,0,96,106,5,37,0,0,97,106,5,39,0,0,98,99,5,
        21,0,0,99,106,3,4,2,10,100,101,5,24,0,0,101,106,3,4,2,9,102,103,
        5,35,0,0,103,104,5,7,0,0,104,106,3,4,2,2,105,88,1,0,0,0,105,90,1,
        0,0,0,105,91,1,0,0,0,105,95,1,0,0,0,105,96,1,0,0,0,105,97,1,0,0,
        0,105,98,1,0,0,0,105,100,1,0,0,0,105,102,1,0,0,0,106,133,1,0,0,0,
        107,108,10,8,0,0,108,109,7,1,0,0,109,132,3,4,2,9,110,111,10,7,0,
        0,111,112,7,2,0,0,112,132,3,4,2,8,113,114,10,6,0,0,114,115,7,3,0,
        0,115,132,3,4,2,7,116,117,10,5,0,0,117,118,7,4,0,0,118,132,3,4,2,
        6,119,120,10,4,0,0,120,121,5,27,0,0,121,132,3,4,2,5,122,123,10,3,
        0,0,123,124,5,28,0,0,124,132,3,4,2,4,125,126,10,1,0,0,126,127,5,
        15,0,0,127,128,3,4,2,0,128,129,5,14,0,0,129,130,3,4,2,1,130,132,
        1,0,0,0,131,107,1,0,0,0,131,110,1,0,0,0,131,113,1,0,0,0,131,116,
        1,0,0,0,131,119,1,0,0,0,131,122,1,0,0,0,131,125,1,0,0,0,132,135,
        1,0,0,0,133,131,1,0,0,0,133,134,1,0,0,0,134,5,1,0,0,0,135,133,1,
        0,0,0,136,141,5,8,0,0,137,141,5,9,0,0,138,141,5,10,0,0,139,141,5,
        11,0,0,140,136,1,0,0,0,140,137,1,0,0,0,140,138,1,0,0,0,140,139,1,
        0,0,0,141,7,1,0,0,0,11,11,20,32,46,56,68,86,105,131,133,140
    ]

class ExprParser ( Parser ):

    grammarFileName = "Expr.g4"

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    sharedContextCache = PredictionContextCache()

    literalNames = [ "<INVALID>", "'{'", "'}'", "'('", "')'", "'true'", 
                     "'false'", "'='", "'int'", "'float'", "'string'", "'bool'", 
                     "';'", "','", "':'", "'?'", "'.'", "'*'", "'/'", "'%'", 
                     "'+'", "'-'", "'<'", "'>'", "'!'", "'=='", "'!='", 
                     "'&&'", "'||'", "'read'", "'write'", "'if'", "'else'", 
                     "'while'", "'for'" ]

    symbolicNames = [ "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "INT_KEYWORD", "FLOAT_KEYWORD", "STRING_KEYWORD", 
                      "BOOL_KEYWORD", "SEMI", "COMMA", "COLON", "QUESTION_MARK", 
                      "CON", "MUL", "DIV", "MOD", "ADD", "SUB", "LES", "GRE", 
                      "NEG", "EQ", "NEQ", "AND", "OR", "READ", "WRITE", 
                      "IF", "ELSE", "WHILE", "FOR", "IDENTIFIER", "INT", 
                      "FLOAT", "BOOL", "STRING", "WS", "COMMENT", "LINE_COMMENT" ]

    RULE_program = 0
    RULE_statement = 1
    RULE_expr = 2
    RULE_primitiveType = 3

    ruleNames =  [ "program", "statement", "expr", "primitiveType" ]

    EOF = Token.EOF
    T__0=1
    T__1=2
    T__2=3
    T__3=4
    T__4=5
    T__5=6
    T__6=7
    INT_KEYWORD=8
    FLOAT_KEYWORD=9
    STRING_KEYWORD=10
    BOOL_KEYWORD=11
    SEMI=12
    COMMA=13
    COLON=14
    QUESTION_MARK=15
    CON=16
    MUL=17
    DIV=18
    MOD=19
    ADD=20
    SUB=21
    LES=22
    GRE=23
    NEG=24
    EQ=25
    NEQ=26
    AND=27
    OR=28
    READ=29
    WRITE=30
    IF=31
    ELSE=32
    WHILE=33
    FOR=34
    IDENTIFIER=35
    INT=36
    FLOAT=37
    BOOL=38
    STRING=39
    WS=40
    COMMENT=41
    LINE_COMMENT=42

    def __init__(self, input:TokenStream, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.13.0")
        self._interp = ParserATNSimulator(self, self.atn, self.decisionsToDFA, self.sharedContextCache)
        self._predicates = None




    class ProgramContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def EOF(self):
            return self.getToken(ExprParser.EOF, 0)

        def statement(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(ExprParser.StatementContext)
            else:
                return self.getTypedRuleContext(ExprParser.StatementContext,i)


        def getRuleIndex(self):
            return ExprParser.RULE_program

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterProgram" ):
                listener.enterProgram(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitProgram" ):
                listener.exitProgram(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitProgram" ):
                return visitor.visitProgram(self)
            else:
                return visitor.visitChildren(self)




    def program(self):

        localctx = ExprParser.ProgramContext(self, self._ctx, self.state)
        self.enterRule(localctx, 0, self.RULE_program)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 9 
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while True:
                self.state = 8
                self.statement()
                self.state = 11 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if not ((((_la) & ~0x3f) == 0 and ((1 << _la) & 819820765034) != 0)):
                    break

            self.state = 13
            self.match(ExprParser.EOF)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class StatementContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return ExprParser.RULE_statement

     
        def copyFrom(self, ctx:ParserRuleContext):
            super().copyFrom(ctx)



    class ConditionalStatementContext(StatementContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a ExprParser.StatementContext
            super().__init__(parser)
            self.pos = None # StatementContext
            self.neg = None # StatementContext
            self.copyFrom(ctx)

        def IF(self):
            return self.getToken(ExprParser.IF, 0)
        def expr(self):
            return self.getTypedRuleContext(ExprParser.ExprContext,0)

        def statement(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(ExprParser.StatementContext)
            else:
                return self.getTypedRuleContext(ExprParser.StatementContext,i)

        def ELSE(self):
            return self.getToken(ExprParser.ELSE, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterConditionalStatement" ):
                listener.enterConditionalStatement(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitConditionalStatement" ):
                listener.exitConditionalStatement(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitConditionalStatement" ):
                return visitor.visitConditionalStatement(self)
            else:
                return visitor.visitChildren(self)


    class ExpressionEvaluationContext(StatementContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a ExprParser.StatementContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def expr(self):
            return self.getTypedRuleContext(ExprParser.ExprContext,0)

        def SEMI(self):
            return self.getToken(ExprParser.SEMI, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterExpressionEvaluation" ):
                listener.enterExpressionEvaluation(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitExpressionEvaluation" ):
                listener.exitExpressionEvaluation(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitExpressionEvaluation" ):
                return visitor.visitExpressionEvaluation(self)
            else:
                return visitor.visitChildren(self)


    class ReadValueContext(StatementContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a ExprParser.StatementContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def READ(self):
            return self.getToken(ExprParser.READ, 0)
        def IDENTIFIER(self, i:int=None):
            if i is None:
                return self.getTokens(ExprParser.IDENTIFIER)
            else:
                return self.getToken(ExprParser.IDENTIFIER, i)
        def SEMI(self):
            return self.getToken(ExprParser.SEMI, 0)
        def COMMA(self, i:int=None):
            if i is None:
                return self.getTokens(ExprParser.COMMA)
            else:
                return self.getToken(ExprParser.COMMA, i)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterReadValue" ):
                listener.enterReadValue(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitReadValue" ):
                listener.exitReadValue(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitReadValue" ):
                return visitor.visitReadValue(self)
            else:
                return visitor.visitChildren(self)


    class ForCycleContext(StatementContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a ExprParser.StatementContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def FOR(self):
            return self.getToken(ExprParser.FOR, 0)
        def expr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(ExprParser.ExprContext)
            else:
                return self.getTypedRuleContext(ExprParser.ExprContext,i)

        def SEMI(self, i:int=None):
            if i is None:
                return self.getTokens(ExprParser.SEMI)
            else:
                return self.getToken(ExprParser.SEMI, i)
        def statement(self):
            return self.getTypedRuleContext(ExprParser.StatementContext,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterForCycle" ):
                listener.enterForCycle(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitForCycle" ):
                listener.exitForCycle(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitForCycle" ):
                return visitor.visitForCycle(self)
            else:
                return visitor.visitChildren(self)


    class BlockOfStatementsContext(StatementContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a ExprParser.StatementContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def statement(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(ExprParser.StatementContext)
            else:
                return self.getTypedRuleContext(ExprParser.StatementContext,i)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterBlockOfStatements" ):
                listener.enterBlockOfStatements(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitBlockOfStatements" ):
                listener.exitBlockOfStatements(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitBlockOfStatements" ):
                return visitor.visitBlockOfStatements(self)
            else:
                return visitor.visitChildren(self)


    class EmptyCommandContext(StatementContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a ExprParser.StatementContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def SEMI(self):
            return self.getToken(ExprParser.SEMI, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterEmptyCommand" ):
                listener.enterEmptyCommand(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitEmptyCommand" ):
                listener.exitEmptyCommand(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitEmptyCommand" ):
                return visitor.visitEmptyCommand(self)
            else:
                return visitor.visitChildren(self)


    class WhileCycleContext(StatementContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a ExprParser.StatementContext
            super().__init__(parser)
            self.content = None # StatementContext
            self.copyFrom(ctx)

        def WHILE(self):
            return self.getToken(ExprParser.WHILE, 0)
        def expr(self):
            return self.getTypedRuleContext(ExprParser.ExprContext,0)

        def statement(self):
            return self.getTypedRuleContext(ExprParser.StatementContext,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterWhileCycle" ):
                listener.enterWhileCycle(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitWhileCycle" ):
                listener.exitWhileCycle(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitWhileCycle" ):
                return visitor.visitWhileCycle(self)
            else:
                return visitor.visitChildren(self)


    class WriteValuesContext(StatementContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a ExprParser.StatementContext
            super().__init__(parser)
            self._expr = None # ExprContext
            self.loadVariables = list() # of ExprContexts
            self.copyFrom(ctx)

        def WRITE(self):
            return self.getToken(ExprParser.WRITE, 0)
        def expr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(ExprParser.ExprContext)
            else:
                return self.getTypedRuleContext(ExprParser.ExprContext,i)

        def SEMI(self):
            return self.getToken(ExprParser.SEMI, 0)
        def COMMA(self, i:int=None):
            if i is None:
                return self.getTokens(ExprParser.COMMA)
            else:
                return self.getToken(ExprParser.COMMA, i)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterWriteValues" ):
                listener.enterWriteValues(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitWriteValues" ):
                listener.exitWriteValues(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitWriteValues" ):
                return visitor.visitWriteValues(self)
            else:
                return visitor.visitChildren(self)


    class DeclarationContext(StatementContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a ExprParser.StatementContext
            super().__init__(parser)
            self.type_ = None # PrimitiveTypeContext
            self.names = None # Token
            self._IDENTIFIER = None # Token
            self.additional_names = list() # of Tokens
            self.copyFrom(ctx)

        def SEMI(self):
            return self.getToken(ExprParser.SEMI, 0)
        def primitiveType(self):
            return self.getTypedRuleContext(ExprParser.PrimitiveTypeContext,0)

        def IDENTIFIER(self, i:int=None):
            if i is None:
                return self.getTokens(ExprParser.IDENTIFIER)
            else:
                return self.getToken(ExprParser.IDENTIFIER, i)
        def COMMA(self, i:int=None):
            if i is None:
                return self.getTokens(ExprParser.COMMA)
            else:
                return self.getToken(ExprParser.COMMA, i)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterDeclaration" ):
                listener.enterDeclaration(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitDeclaration" ):
                listener.exitDeclaration(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitDeclaration" ):
                return visitor.visitDeclaration(self)
            else:
                return visitor.visitChildren(self)



    def statement(self):

        localctx = ExprParser.StatementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 2, self.RULE_statement)
        self._la = 0 # Token type
        try:
            self.state = 86
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [1]:
                localctx = ExprParser.BlockOfStatementsContext(self, localctx)
                self.enterOuterAlt(localctx, 1)
                self.state = 15
                self.match(ExprParser.T__0)
                self.state = 16
                self.statement()
                self.state = 20
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while (((_la) & ~0x3f) == 0 and ((1 << _la) & 819820765034) != 0):
                    self.state = 17
                    self.statement()
                    self.state = 22
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 23
                self.match(ExprParser.T__1)
                pass
            elif token in [12]:
                localctx = ExprParser.EmptyCommandContext(self, localctx)
                self.enterOuterAlt(localctx, 2)
                self.state = 25
                self.match(ExprParser.SEMI)
                pass
            elif token in [8, 9, 10, 11]:
                localctx = ExprParser.DeclarationContext(self, localctx)
                self.enterOuterAlt(localctx, 3)
                self.state = 26
                localctx.type_ = self.primitiveType()
                self.state = 27
                localctx.names = self.match(ExprParser.IDENTIFIER)
                self.state = 32
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==13:
                    self.state = 28
                    self.match(ExprParser.COMMA)
                    self.state = 29
                    localctx._IDENTIFIER = self.match(ExprParser.IDENTIFIER)
                    localctx.additional_names.append(localctx._IDENTIFIER)
                    self.state = 34
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 35
                self.match(ExprParser.SEMI)
                pass
            elif token in [3, 5, 6, 21, 24, 35, 36, 37, 39]:
                localctx = ExprParser.ExpressionEvaluationContext(self, localctx)
                self.enterOuterAlt(localctx, 4)
                self.state = 37
                self.expr(0)
                self.state = 38
                self.match(ExprParser.SEMI)
                pass
            elif token in [29]:
                localctx = ExprParser.ReadValueContext(self, localctx)
                self.enterOuterAlt(localctx, 5)
                self.state = 40
                self.match(ExprParser.READ)
                self.state = 41
                self.match(ExprParser.IDENTIFIER)
                self.state = 46
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==13:
                    self.state = 42
                    self.match(ExprParser.COMMA)
                    self.state = 43
                    self.match(ExprParser.IDENTIFIER)
                    self.state = 48
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 49
                self.match(ExprParser.SEMI)
                pass
            elif token in [30]:
                localctx = ExprParser.WriteValuesContext(self, localctx)
                self.enterOuterAlt(localctx, 6)
                self.state = 50
                self.match(ExprParser.WRITE)
                self.state = 51
                self.expr(0)
                self.state = 56
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                while _la==13:
                    self.state = 52
                    self.match(ExprParser.COMMA)
                    self.state = 53
                    localctx._expr = self.expr(0)
                    localctx.loadVariables.append(localctx._expr)
                    self.state = 58
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)

                self.state = 59
                self.match(ExprParser.SEMI)
                pass
            elif token in [31]:
                localctx = ExprParser.ConditionalStatementContext(self, localctx)
                self.enterOuterAlt(localctx, 7)
                self.state = 61
                self.match(ExprParser.IF)
                self.state = 62
                self.match(ExprParser.T__2)
                self.state = 63
                self.expr(0)
                self.state = 64
                self.match(ExprParser.T__3)
                self.state = 65
                localctx.pos = self.statement()
                self.state = 68
                self._errHandler.sync(self)
                la_ = self._interp.adaptivePredict(self._input,5,self._ctx)
                if la_ == 1:
                    self.state = 66
                    self.match(ExprParser.ELSE)
                    self.state = 67
                    localctx.neg = self.statement()


                pass
            elif token in [33]:
                localctx = ExprParser.WhileCycleContext(self, localctx)
                self.enterOuterAlt(localctx, 8)
                self.state = 70
                self.match(ExprParser.WHILE)
                self.state = 71
                self.match(ExprParser.T__2)
                self.state = 72
                self.expr(0)
                self.state = 73
                self.match(ExprParser.T__3)
                self.state = 74
                localctx.content = self.statement()
                pass
            elif token in [34]:
                localctx = ExprParser.ForCycleContext(self, localctx)
                self.enterOuterAlt(localctx, 9)
                self.state = 76
                self.match(ExprParser.FOR)
                self.state = 77
                self.match(ExprParser.T__2)
                self.state = 78
                self.expr(0)
                self.state = 79
                self.match(ExprParser.SEMI)
                self.state = 80
                self.expr(0)
                self.state = 81
                self.match(ExprParser.SEMI)
                self.state = 82
                self.expr(0)
                self.state = 83
                self.match(ExprParser.T__3)
                self.state = 84
                self.statement()
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class ExprContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser


        def getRuleIndex(self):
            return ExprParser.RULE_expr

     
        def copyFrom(self, ctx:ParserRuleContext):
            super().copyFrom(ctx)


    class MulDivModContext(ExprContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a ExprParser.ExprContext
            super().__init__(parser)
            self.left = None # ExprContext
            self.op = None # Token
            self.right = None # ExprContext
            self.copyFrom(ctx)

        def expr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(ExprParser.ExprContext)
            else:
                return self.getTypedRuleContext(ExprParser.ExprContext,i)

        def MUL(self):
            return self.getToken(ExprParser.MUL, 0)
        def DIV(self):
            return self.getToken(ExprParser.DIV, 0)
        def MOD(self):
            return self.getToken(ExprParser.MOD, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterMulDivMod" ):
                listener.enterMulDivMod(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitMulDivMod" ):
                listener.exitMulDivMod(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitMulDivMod" ):
                return visitor.visitMulDivMod(self)
            else:
                return visitor.visitChildren(self)


    class IdentifierContext(ExprContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a ExprParser.ExprContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def IDENTIFIER(self):
            return self.getToken(ExprParser.IDENTIFIER, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterIdentifier" ):
                listener.enterIdentifier(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitIdentifier" ):
                listener.exitIdentifier(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitIdentifier" ):
                return visitor.visitIdentifier(self)
            else:
                return visitor.visitChildren(self)


    class ParensContext(ExprContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a ExprParser.ExprContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def expr(self):
            return self.getTypedRuleContext(ExprParser.ExprContext,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterParens" ):
                listener.enterParens(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitParens" ):
                listener.exitParens(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitParens" ):
                return visitor.visitParens(self)
            else:
                return visitor.visitChildren(self)


    class ComparisonContext(ExprContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a ExprParser.ExprContext
            super().__init__(parser)
            self.left = None # ExprContext
            self.op = None # Token
            self.right = None # ExprContext
            self.copyFrom(ctx)

        def expr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(ExprParser.ExprContext)
            else:
                return self.getTypedRuleContext(ExprParser.ExprContext,i)

        def EQ(self):
            return self.getToken(ExprParser.EQ, 0)
        def NEQ(self):
            return self.getToken(ExprParser.NEQ, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterComparison" ):
                listener.enterComparison(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitComparison" ):
                listener.exitComparison(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitComparison" ):
                return visitor.visitComparison(self)
            else:
                return visitor.visitChildren(self)


    class BoolContext(ExprContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a ExprParser.ExprContext
            super().__init__(parser)
            self.copyFrom(ctx)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterBool" ):
                listener.enterBool(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitBool" ):
                listener.exitBool(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitBool" ):
                return visitor.visitBool(self)
            else:
                return visitor.visitChildren(self)


    class StringContext(ExprContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a ExprParser.ExprContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def STRING(self):
            return self.getToken(ExprParser.STRING, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterString" ):
                listener.enterString(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitString" ):
                listener.exitString(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitString" ):
                return visitor.visitString(self)
            else:
                return visitor.visitChildren(self)


    class AssignmentContext(ExprContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a ExprParser.ExprContext
            super().__init__(parser)
            self.left = None # Token
            self.right = None # ExprContext
            self.copyFrom(ctx)

        def IDENTIFIER(self):
            return self.getToken(ExprParser.IDENTIFIER, 0)
        def expr(self):
            return self.getTypedRuleContext(ExprParser.ExprContext,0)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterAssignment" ):
                listener.enterAssignment(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitAssignment" ):
                listener.exitAssignment(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitAssignment" ):
                return visitor.visitAssignment(self)
            else:
                return visitor.visitChildren(self)


    class LogicalAndContext(ExprContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a ExprParser.ExprContext
            super().__init__(parser)
            self.left = None # ExprContext
            self.right = None # ExprContext
            self.copyFrom(ctx)

        def AND(self):
            return self.getToken(ExprParser.AND, 0)
        def expr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(ExprParser.ExprContext)
            else:
                return self.getTypedRuleContext(ExprParser.ExprContext,i)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterLogicalAnd" ):
                listener.enterLogicalAnd(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitLogicalAnd" ):
                listener.exitLogicalAnd(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitLogicalAnd" ):
                return visitor.visitLogicalAnd(self)
            else:
                return visitor.visitChildren(self)


    class FloatContext(ExprContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a ExprParser.ExprContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def FLOAT(self):
            return self.getToken(ExprParser.FLOAT, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterFloat" ):
                listener.enterFloat(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitFloat" ):
                listener.exitFloat(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitFloat" ):
                return visitor.visitFloat(self)
            else:
                return visitor.visitChildren(self)


    class TernaryOperatorContext(ExprContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a ExprParser.ExprContext
            super().__init__(parser)
            self.left = None # ExprContext
            self.expression1 = None # ExprContext
            self.expression2 = None # ExprContext
            self.copyFrom(ctx)

        def QUESTION_MARK(self):
            return self.getToken(ExprParser.QUESTION_MARK, 0)
        def COLON(self):
            return self.getToken(ExprParser.COLON, 0)
        def expr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(ExprParser.ExprContext)
            else:
                return self.getTypedRuleContext(ExprParser.ExprContext,i)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterTernaryOperator" ):
                listener.enterTernaryOperator(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitTernaryOperator" ):
                listener.exitTernaryOperator(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitTernaryOperator" ):
                return visitor.visitTernaryOperator(self)
            else:
                return visitor.visitChildren(self)


    class IntContext(ExprContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a ExprParser.ExprContext
            super().__init__(parser)
            self.copyFrom(ctx)

        def INT(self):
            return self.getToken(ExprParser.INT, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterInt" ):
                listener.enterInt(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitInt" ):
                listener.exitInt(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitInt" ):
                return visitor.visitInt(self)
            else:
                return visitor.visitChildren(self)


    class AddSubConContext(ExprContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a ExprParser.ExprContext
            super().__init__(parser)
            self.left = None # ExprContext
            self.op = None # Token
            self.right = None # ExprContext
            self.copyFrom(ctx)

        def expr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(ExprParser.ExprContext)
            else:
                return self.getTypedRuleContext(ExprParser.ExprContext,i)

        def ADD(self):
            return self.getToken(ExprParser.ADD, 0)
        def SUB(self):
            return self.getToken(ExprParser.SUB, 0)
        def CON(self):
            return self.getToken(ExprParser.CON, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterAddSubCon" ):
                listener.enterAddSubCon(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitAddSubCon" ):
                listener.exitAddSubCon(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitAddSubCon" ):
                return visitor.visitAddSubCon(self)
            else:
                return visitor.visitChildren(self)


    class UnaryMinusContext(ExprContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a ExprParser.ExprContext
            super().__init__(parser)
            self.prefix = None # Token
            self.copyFrom(ctx)

        def expr(self):
            return self.getTypedRuleContext(ExprParser.ExprContext,0)

        def SUB(self):
            return self.getToken(ExprParser.SUB, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterUnaryMinus" ):
                listener.enterUnaryMinus(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitUnaryMinus" ):
                listener.exitUnaryMinus(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitUnaryMinus" ):
                return visitor.visitUnaryMinus(self)
            else:
                return visitor.visitChildren(self)


    class RelationalContext(ExprContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a ExprParser.ExprContext
            super().__init__(parser)
            self.left = None # ExprContext
            self.op = None # Token
            self.right = None # ExprContext
            self.copyFrom(ctx)

        def expr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(ExprParser.ExprContext)
            else:
                return self.getTypedRuleContext(ExprParser.ExprContext,i)

        def LES(self):
            return self.getToken(ExprParser.LES, 0)
        def GRE(self):
            return self.getToken(ExprParser.GRE, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterRelational" ):
                listener.enterRelational(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitRelational" ):
                listener.exitRelational(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitRelational" ):
                return visitor.visitRelational(self)
            else:
                return visitor.visitChildren(self)


    class LogicalOrContext(ExprContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a ExprParser.ExprContext
            super().__init__(parser)
            self.left = None # ExprContext
            self.right = None # ExprContext
            self.copyFrom(ctx)

        def OR(self):
            return self.getToken(ExprParser.OR, 0)
        def expr(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(ExprParser.ExprContext)
            else:
                return self.getTypedRuleContext(ExprParser.ExprContext,i)


        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterLogicalOr" ):
                listener.enterLogicalOr(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitLogicalOr" ):
                listener.exitLogicalOr(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitLogicalOr" ):
                return visitor.visitLogicalOr(self)
            else:
                return visitor.visitChildren(self)


    class LogicNotContext(ExprContext):

        def __init__(self, parser, ctx:ParserRuleContext): # actually a ExprParser.ExprContext
            super().__init__(parser)
            self.prefix = None # Token
            self.copyFrom(ctx)

        def expr(self):
            return self.getTypedRuleContext(ExprParser.ExprContext,0)

        def NEG(self):
            return self.getToken(ExprParser.NEG, 0)

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterLogicNot" ):
                listener.enterLogicNot(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitLogicNot" ):
                listener.exitLogicNot(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitLogicNot" ):
                return visitor.visitLogicNot(self)
            else:
                return visitor.visitChildren(self)



    def expr(self, _p:int=0):
        _parentctx = self._ctx
        _parentState = self.state
        localctx = ExprParser.ExprContext(self, self._ctx, _parentState)
        _prevctx = localctx
        _startState = 4
        self.enterRecursionRule(localctx, 4, self.RULE_expr, _p)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 105
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,7,self._ctx)
            if la_ == 1:
                localctx = ExprParser.IdentifierContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx

                self.state = 89
                self.match(ExprParser.IDENTIFIER)
                pass

            elif la_ == 2:
                localctx = ExprParser.BoolContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx
                self.state = 90
                _la = self._input.LA(1)
                if not(_la==5 or _la==6):
                    self._errHandler.recoverInline(self)
                else:
                    self._errHandler.reportMatch(self)
                    self.consume()
                pass

            elif la_ == 3:
                localctx = ExprParser.ParensContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx
                self.state = 91
                self.match(ExprParser.T__2)
                self.state = 92
                self.expr(0)
                self.state = 93
                self.match(ExprParser.T__3)
                pass

            elif la_ == 4:
                localctx = ExprParser.IntContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx
                self.state = 95
                self.match(ExprParser.INT)
                pass

            elif la_ == 5:
                localctx = ExprParser.FloatContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx
                self.state = 96
                self.match(ExprParser.FLOAT)
                pass

            elif la_ == 6:
                localctx = ExprParser.StringContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx
                self.state = 97
                self.match(ExprParser.STRING)
                pass

            elif la_ == 7:
                localctx = ExprParser.UnaryMinusContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx
                self.state = 98
                localctx.prefix = self.match(ExprParser.SUB)
                self.state = 99
                self.expr(10)
                pass

            elif la_ == 8:
                localctx = ExprParser.LogicNotContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx
                self.state = 100
                localctx.prefix = self.match(ExprParser.NEG)
                self.state = 101
                self.expr(9)
                pass

            elif la_ == 9:
                localctx = ExprParser.AssignmentContext(self, localctx)
                self._ctx = localctx
                _prevctx = localctx
                self.state = 102
                localctx.left = self.match(ExprParser.IDENTIFIER)
                self.state = 103
                self.match(ExprParser.T__6)
                self.state = 104
                localctx.right = self.expr(2)
                pass


            self._ctx.stop = self._input.LT(-1)
            self.state = 133
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,9,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    if self._parseListeners is not None:
                        self.triggerExitRuleEvent()
                    _prevctx = localctx
                    self.state = 131
                    self._errHandler.sync(self)
                    la_ = self._interp.adaptivePredict(self._input,8,self._ctx)
                    if la_ == 1:
                        localctx = ExprParser.MulDivModContext(self, ExprParser.ExprContext(self, _parentctx, _parentState))
                        localctx.left = _prevctx
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expr)
                        self.state = 107
                        if not self.precpred(self._ctx, 8):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 8)")
                        self.state = 108
                        localctx.op = self._input.LT(1)
                        _la = self._input.LA(1)
                        if not((((_la) & ~0x3f) == 0 and ((1 << _la) & 917504) != 0)):
                            localctx.op = self._errHandler.recoverInline(self)
                        else:
                            self._errHandler.reportMatch(self)
                            self.consume()
                        self.state = 109
                        localctx.right = self.expr(9)
                        pass

                    elif la_ == 2:
                        localctx = ExprParser.AddSubConContext(self, ExprParser.ExprContext(self, _parentctx, _parentState))
                        localctx.left = _prevctx
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expr)
                        self.state = 110
                        if not self.precpred(self._ctx, 7):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 7)")
                        self.state = 111
                        localctx.op = self._input.LT(1)
                        _la = self._input.LA(1)
                        if not((((_la) & ~0x3f) == 0 and ((1 << _la) & 3211264) != 0)):
                            localctx.op = self._errHandler.recoverInline(self)
                        else:
                            self._errHandler.reportMatch(self)
                            self.consume()
                        self.state = 112
                        localctx.right = self.expr(8)
                        pass

                    elif la_ == 3:
                        localctx = ExprParser.RelationalContext(self, ExprParser.ExprContext(self, _parentctx, _parentState))
                        localctx.left = _prevctx
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expr)
                        self.state = 113
                        if not self.precpred(self._ctx, 6):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 6)")
                        self.state = 114
                        localctx.op = self._input.LT(1)
                        _la = self._input.LA(1)
                        if not(_la==22 or _la==23):
                            localctx.op = self._errHandler.recoverInline(self)
                        else:
                            self._errHandler.reportMatch(self)
                            self.consume()
                        self.state = 115
                        localctx.right = self.expr(7)
                        pass

                    elif la_ == 4:
                        localctx = ExprParser.ComparisonContext(self, ExprParser.ExprContext(self, _parentctx, _parentState))
                        localctx.left = _prevctx
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expr)
                        self.state = 116
                        if not self.precpred(self._ctx, 5):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 5)")
                        self.state = 117
                        localctx.op = self._input.LT(1)
                        _la = self._input.LA(1)
                        if not(_la==25 or _la==26):
                            localctx.op = self._errHandler.recoverInline(self)
                        else:
                            self._errHandler.reportMatch(self)
                            self.consume()
                        self.state = 118
                        localctx.right = self.expr(6)
                        pass

                    elif la_ == 5:
                        localctx = ExprParser.LogicalAndContext(self, ExprParser.ExprContext(self, _parentctx, _parentState))
                        localctx.left = _prevctx
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expr)
                        self.state = 119
                        if not self.precpred(self._ctx, 4):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 4)")
                        self.state = 120
                        self.match(ExprParser.AND)
                        self.state = 121
                        localctx.right = self.expr(5)
                        pass

                    elif la_ == 6:
                        localctx = ExprParser.LogicalOrContext(self, ExprParser.ExprContext(self, _parentctx, _parentState))
                        localctx.left = _prevctx
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expr)
                        self.state = 122
                        if not self.precpred(self._ctx, 3):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 3)")
                        self.state = 123
                        self.match(ExprParser.OR)
                        self.state = 124
                        localctx.right = self.expr(4)
                        pass

                    elif la_ == 7:
                        localctx = ExprParser.TernaryOperatorContext(self, ExprParser.ExprContext(self, _parentctx, _parentState))
                        localctx.left = _prevctx
                        self.pushNewRecursionContext(localctx, _startState, self.RULE_expr)
                        self.state = 125
                        if not self.precpred(self._ctx, 1):
                            from antlr4.error.Errors import FailedPredicateException
                            raise FailedPredicateException(self, "self.precpred(self._ctx, 1)")
                        self.state = 126
                        self.match(ExprParser.QUESTION_MARK)
                        self.state = 127
                        localctx.expression1 = self.expr(0)
                        self.state = 128
                        self.match(ExprParser.COLON)
                        self.state = 129
                        localctx.expression2 = self.expr(1)
                        pass

             
                self.state = 135
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,9,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.unrollRecursionContexts(_parentctx)
        return localctx


    class PrimitiveTypeContext(ParserRuleContext):
        __slots__ = 'parser'

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.type_ = None # Token

        def INT_KEYWORD(self):
            return self.getToken(ExprParser.INT_KEYWORD, 0)

        def FLOAT_KEYWORD(self):
            return self.getToken(ExprParser.FLOAT_KEYWORD, 0)

        def STRING_KEYWORD(self):
            return self.getToken(ExprParser.STRING_KEYWORD, 0)

        def BOOL_KEYWORD(self):
            return self.getToken(ExprParser.BOOL_KEYWORD, 0)

        def getRuleIndex(self):
            return ExprParser.RULE_primitiveType

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterPrimitiveType" ):
                listener.enterPrimitiveType(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitPrimitiveType" ):
                listener.exitPrimitiveType(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitPrimitiveType" ):
                return visitor.visitPrimitiveType(self)
            else:
                return visitor.visitChildren(self)




    def primitiveType(self):

        localctx = ExprParser.PrimitiveTypeContext(self, self._ctx, self.state)
        self.enterRule(localctx, 6, self.RULE_primitiveType)
        try:
            self.state = 140
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [8]:
                self.enterOuterAlt(localctx, 1)
                self.state = 136
                localctx.type_ = self.match(ExprParser.INT_KEYWORD)
                pass
            elif token in [9]:
                self.enterOuterAlt(localctx, 2)
                self.state = 137
                localctx.type_ = self.match(ExprParser.FLOAT_KEYWORD)
                pass
            elif token in [10]:
                self.enterOuterAlt(localctx, 3)
                self.state = 138
                localctx.type_ = self.match(ExprParser.STRING_KEYWORD)
                pass
            elif token in [11]:
                self.enterOuterAlt(localctx, 4)
                self.state = 139
                localctx.type_ = self.match(ExprParser.BOOL_KEYWORD)
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx



    def sempred(self, localctx:RuleContext, ruleIndex:int, predIndex:int):
        if self._predicates == None:
            self._predicates = dict()
        self._predicates[2] = self.expr_sempred
        pred = self._predicates.get(ruleIndex, None)
        if pred is None:
            raise Exception("No predicate with index:" + str(ruleIndex))
        else:
            return pred(localctx, predIndex)

    def expr_sempred(self, localctx:ExprContext, predIndex:int):
            if predIndex == 0:
                return self.precpred(self._ctx, 8)
         

            if predIndex == 1:
                return self.precpred(self._ctx, 7)
         

            if predIndex == 2:
                return self.precpred(self._ctx, 6)
         

            if predIndex == 3:
                return self.precpred(self._ctx, 5)
         

            if predIndex == 4:
                return self.precpred(self._ctx, 4)
         

            if predIndex == 5:
                return self.precpred(self._ctx, 3)
         

            if predIndex == 6:
                return self.precpred(self._ctx, 1)
         




