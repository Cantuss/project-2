# Generated from Expr.g4 by ANTLR 4.13.0
from antlr4 import *
if "." in __name__:
    from .ExprParser import ExprParser
else:
    from ExprParser import ExprParser

# This class defines a complete generic visitor for a parse tree produced by ExprParser.

class ExprVisitor(ParseTreeVisitor):

    # Visit a parse tree produced by ExprParser#program.
    def visitProgram(self, ctx:ExprParser.ProgramContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by ExprParser#blockOfStatements.
    def visitBlockOfStatements(self, ctx:ExprParser.BlockOfStatementsContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by ExprParser#emptyCommand.
    def visitEmptyCommand(self, ctx:ExprParser.EmptyCommandContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by ExprParser#declaration.
    def visitDeclaration(self, ctx:ExprParser.DeclarationContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by ExprParser#expressionEvaluation.
    def visitExpressionEvaluation(self, ctx:ExprParser.ExpressionEvaluationContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by ExprParser#readValue.
    def visitReadValue(self, ctx:ExprParser.ReadValueContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by ExprParser#writeValues.
    def visitWriteValues(self, ctx:ExprParser.WriteValuesContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by ExprParser#conditionalStatement.
    def visitConditionalStatement(self, ctx:ExprParser.ConditionalStatementContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by ExprParser#whileCycle.
    def visitWhileCycle(self, ctx:ExprParser.WhileCycleContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by ExprParser#forCycle.
    def visitForCycle(self, ctx:ExprParser.ForCycleContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by ExprParser#mulDivMod.
    def visitMulDivMod(self, ctx:ExprParser.MulDivModContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by ExprParser#identifier.
    def visitIdentifier(self, ctx:ExprParser.IdentifierContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by ExprParser#parens.
    def visitParens(self, ctx:ExprParser.ParensContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by ExprParser#comparison.
    def visitComparison(self, ctx:ExprParser.ComparisonContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by ExprParser#bool.
    def visitBool(self, ctx:ExprParser.BoolContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by ExprParser#string.
    def visitString(self, ctx:ExprParser.StringContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by ExprParser#assignment.
    def visitAssignment(self, ctx:ExprParser.AssignmentContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by ExprParser#logicalAnd.
    def visitLogicalAnd(self, ctx:ExprParser.LogicalAndContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by ExprParser#float.
    def visitFloat(self, ctx:ExprParser.FloatContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by ExprParser#ternaryOperator.
    def visitTernaryOperator(self, ctx:ExprParser.TernaryOperatorContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by ExprParser#int.
    def visitInt(self, ctx:ExprParser.IntContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by ExprParser#addSubCon.
    def visitAddSubCon(self, ctx:ExprParser.AddSubConContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by ExprParser#unaryMinus.
    def visitUnaryMinus(self, ctx:ExprParser.UnaryMinusContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by ExprParser#relational.
    def visitRelational(self, ctx:ExprParser.RelationalContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by ExprParser#logicalOr.
    def visitLogicalOr(self, ctx:ExprParser.LogicalOrContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by ExprParser#logicNot.
    def visitLogicNot(self, ctx:ExprParser.LogicNotContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by ExprParser#primitiveType.
    def visitPrimitiveType(self, ctx:ExprParser.PrimitiveTypeContext):
        return self.visitChildren(ctx)



del ExprParser