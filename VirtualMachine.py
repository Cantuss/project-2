class VirtualMachine:
    def __init__(self,code):
        self.code = []
        self.stack = []
        self.memory = {}
        self.labelsDictionary = {}
        self.labelsVisited = []
        
        code = code.split('\n')

        #print(code)
        for line in code:
            #print((line.split()))
            self.code.append(line.split())
        #print(self.code)
        self.code.pop()
    
    def run(self, startFrom):
        jumpToThisLabel = None
        lineCount = 0
        for line in self.code:
            #print(line[0])
            if lineCount < startFrom:
                lineCount += 1
                continue

            lineCount += 1


            if jumpToThisLabel is not None:
                #if line[0] == "label" and line[1] in self.labelsDictionary and self.labelsDictionary[str(line[1])] == jumpToThisLabel:
                #    jumpToThisLabel = None
                #    continue
                #else:
                #    continue
                if line[0] != "label":
                    continue
                elif line[0] == "label" and line[1] != jumpToThisLabel:
                    #self.labelsVisited.append(str(jumpToThisLabel))
                    continue
                elif line[0] == "label" and line[1] == jumpToThisLabel:
                    jumpToThisLabel = None
                    #print("skocil jsem na "+ str(line[1]))
                    #self.labelsVisited.append(jumpToThisLabel)
                    continue

            if line[0] == "push":
                if line[1] == "I":
                    self.stack.append(int(line[2]))     
                elif line[1] == "F":
                    self.stack.append(float(line[2]))
                elif line[1] == "B":
                    #self.stack.append(bool(line[2]))
                    if line[2] == "true":
                        self.stack.append(True)
                    else:
                        self.stack.append(False)
                elif line[1] == "S":
                    string = "".join(line[2:])
                    if string == '""':
                        self.stack.append('""')
                    else:
                        self.stack.append(string)
                        

            elif line[0] == "print":
                toBePrinted = []
                for x in range(0,int(line[1])):
                    #print("printing"+str(x))
                    toBePrinted.append(self.stack.pop())
                    #print(self.stack.pop())
                toBePrinted.reverse()
                #print(toBePrinted)
                for value in toBePrinted:
                    print(value)

            elif line[0] == "save":
                #print(self.stack)
                value = self.stack.pop()
                self.memory[line[1]] = value

            elif line[0] == "load":
                self.stack.append(self.memory[line[1]])

            elif line[0] in ["add","mul","sub","div"]:
                right = self.stack.pop()
                left = self.stack.pop()

                if line[0] == "add":
                    if left is int and right is int:
                        self.stack.append(left+right)
                    elif left is not bool and left is not str:
                        #self.stack.append((float)(left is int? (int)left:(float)left)+ (float)(right is int ? (int)right : (float)right))
                        if left is int:
                            left = int(left)
                        else:
                            left =float(left)
                        if right is int:
                            right = int(right)
                        else:
                            right = float(right)
                        self.stack.append(float(left+right))

                elif line[0] == "mul":
                    if left is int and right is int:
                        self.stack.append(left*right)
                    elif left is not bool and left is not str:
                        #self.stack.append((float)(left is int? (int)left:(float)left)+ (float)(right is int ? (int)right : (float)right))
                        if left is int:
                            left = int(left)
                        else:
                            left =float(left)
                        if right is int:
                            right = int(right)
                        else:
                            right = float(right)
                        self.stack.append(float(left*right))

                elif line[0] == "sub":
                    if left is int and right is int:
                        self.stack.append(left-right)
                    elif left is not bool and left is not str:
                        #self.stack.append((float)(left is int? (int)left:(float)left)+ (float)(right is int ? (int)right : (float)right))
                        if left is int:
                            left = int(left)
                        else:
                            left =float(left)
                        if right is int:
                            right = int(right)
                        else:
                            right = float(right)
                        self.stack.append(float(left-right))
                    
                elif line[0] == "div":
                    if left is int and right is int:
                        self.stack.append(left/right)
                    elif left is not bool and left is not str:
                        #self.stack.append((float)(left is int? (int)left:(float)left)+ (float)(right is int ? (int)right : (float)right))
                        if left is int:
                            left = int(left)
                        else:
                            left =float(left)
                        if right is int:
                            right = int(right)
                        else:
                            right = float(right)
                        self.stack.append(float(left/right))

                    #elif line[0] == "mod":

            elif line[0] == "itof":
                value = self.stack.pop()
                value = float(value)
                self.stack.append(value)
                        

            elif line[0] == "mod":
                right = self.stack.pop()
                left = self.stack.pop()
                self.stack.append(left%right)

            elif line[0] == "pop":
                self.stack.pop()
                
            elif line[0] == "concat":
                right = self.stack.pop()
                left = self.stack.pop()
                string = "".join((left.replace('"',""),right.replace('"',"")))
                self.stack.append(string)
                
            elif line[0] == "uminus":
                value = self.stack.pop()
                self.stack.append(-value)

            elif line[0] == "read":
                typeVar = line[1]
                if typeVar == "I":
                    value = input("Enter an Integer: ")
                    self.stack.append(int(value))
                elif typeVar == "F":
                    value = input("Enter Float: ")
                    self.stack.append(float(value))
                elif typeVar == "B":
                    value = input("Enter Boolean: ")
                    self.stack.append(bool(value))
                elif typeVar == "S":
                    value = input("Enter String: ")
                    self.stack.append(str(value))
            
            elif line[0] == "itof":
                value = self.stack.pop()
                self.stack.append(float(value));

            elif line[0] == "lt":
                right = self.stack.pop()
                left = self.stack.pop()
                if left < right:
                    self.stack.append(True)
                else:
                    self.stack.append(False)

            elif line[0] == "gt":
                right = self.stack.pop()
                left = self.stack.pop()
                if left < right:
                    self.stack.append(False)
                else:
                    self.stack.append(True)

            elif line[0] == "eq":
                right = self.stack.pop()
                left = self.stack.pop()
                if left == right:
                    self.stack.append(True)
                else:
                    self.stack.append(False)

            elif line[0] == "not":
                value = self.stack.pop()
                value = not value
                self.stack.append(value)

            elif line[0] == "and":
                #print(self.stack)
                right = self.stack.pop()
                left = self.stack.pop()
                if left and right:
                    self.stack.append(True)
                else:
                    self.stack.append(False)

            elif line[0] == "or":
                right = self.stack.pop()
                left = self.stack.pop()
                if left or right:
                    self.stack.append(True)
                else:
                    self.stack.append(False)

            elif line[0] == "label":
                if line[1] not in self.labelsVisited:
                    self.labelsVisited.append(line[1])
                self.labelsDictionary[str(line[1])] = lineCount
                #print("label "+str(line[1]))
            
            elif line[0] == "jmp":
                if line[1] in self.labelsVisited:
                    #print("tahle label uz byla" + str(line[1]))
                    self.run(self.labelsDictionary[str(line[1])])
                    exit()
                else:
                    jumpToThisLabel = line[1]

            elif line[0] == "fjmp":
                if line[1] in self.labelsVisited:
                    #print("fjmp, tahle label uz byla")
                    #print("fjmp skace na"+ str(line[1]))
                    self.run(self.labelsDictionary[str(line[1])])
                    exit()
                else:
                    if self.stack.pop() == False:
                        jumpToThisLabel = line[1]
                        #print("fjmp skace na " + str(line[1]))

            #print(self.labelsVisited)

        #print(self.stack)