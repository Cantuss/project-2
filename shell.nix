 let
  pkgs = import <nixpkgs> {};
in pkgs.mkShell {
  packages = [
    (pkgs.python3.withPackages (python-pkgs: [
	python-pkgs.antlr4-python3-runtime          
    ]))
#    (python311.withPackages(ps: with ps; [ antlr4-python3-runtime ]))
  ];
}
