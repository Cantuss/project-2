# Generated from Expr.g4 by ANTLR 4.13.0
from antlr4 import *
if "." in __name__:
    from .ExprParser import ExprParser
else:
    from ExprParser import ExprParser

# This class defines a complete listener for a parse tree produced by ExprParser.
class ExprListener(ParseTreeListener):

    # Enter a parse tree produced by ExprParser#program.
    def enterProgram(self, ctx:ExprParser.ProgramContext):
        pass

    # Exit a parse tree produced by ExprParser#program.
    def exitProgram(self, ctx:ExprParser.ProgramContext):
        pass


    # Enter a parse tree produced by ExprParser#blockOfStatements.
    def enterBlockOfStatements(self, ctx:ExprParser.BlockOfStatementsContext):
        pass

    # Exit a parse tree produced by ExprParser#blockOfStatements.
    def exitBlockOfStatements(self, ctx:ExprParser.BlockOfStatementsContext):
        pass


    # Enter a parse tree produced by ExprParser#emptyCommand.
    def enterEmptyCommand(self, ctx:ExprParser.EmptyCommandContext):
        pass

    # Exit a parse tree produced by ExprParser#emptyCommand.
    def exitEmptyCommand(self, ctx:ExprParser.EmptyCommandContext):
        pass


    # Enter a parse tree produced by ExprParser#declaration.
    def enterDeclaration(self, ctx:ExprParser.DeclarationContext):
        pass

    # Exit a parse tree produced by ExprParser#declaration.
    def exitDeclaration(self, ctx:ExprParser.DeclarationContext):
        pass


    # Enter a parse tree produced by ExprParser#expressionEvaluation.
    def enterExpressionEvaluation(self, ctx:ExprParser.ExpressionEvaluationContext):
        pass

    # Exit a parse tree produced by ExprParser#expressionEvaluation.
    def exitExpressionEvaluation(self, ctx:ExprParser.ExpressionEvaluationContext):
        pass


    # Enter a parse tree produced by ExprParser#readValue.
    def enterReadValue(self, ctx:ExprParser.ReadValueContext):
        pass

    # Exit a parse tree produced by ExprParser#readValue.
    def exitReadValue(self, ctx:ExprParser.ReadValueContext):
        pass


    # Enter a parse tree produced by ExprParser#writeValues.
    def enterWriteValues(self, ctx:ExprParser.WriteValuesContext):
        pass

    # Exit a parse tree produced by ExprParser#writeValues.
    def exitWriteValues(self, ctx:ExprParser.WriteValuesContext):
        pass


    # Enter a parse tree produced by ExprParser#conditionalStatement.
    def enterConditionalStatement(self, ctx:ExprParser.ConditionalStatementContext):
        pass

    # Exit a parse tree produced by ExprParser#conditionalStatement.
    def exitConditionalStatement(self, ctx:ExprParser.ConditionalStatementContext):
        pass


    # Enter a parse tree produced by ExprParser#whileCycle.
    def enterWhileCycle(self, ctx:ExprParser.WhileCycleContext):
        pass

    # Exit a parse tree produced by ExprParser#whileCycle.
    def exitWhileCycle(self, ctx:ExprParser.WhileCycleContext):
        pass


    # Enter a parse tree produced by ExprParser#forCycle.
    def enterForCycle(self, ctx:ExprParser.ForCycleContext):
        pass

    # Exit a parse tree produced by ExprParser#forCycle.
    def exitForCycle(self, ctx:ExprParser.ForCycleContext):
        pass


    # Enter a parse tree produced by ExprParser#mulDivMod.
    def enterMulDivMod(self, ctx:ExprParser.MulDivModContext):
        pass

    # Exit a parse tree produced by ExprParser#mulDivMod.
    def exitMulDivMod(self, ctx:ExprParser.MulDivModContext):
        pass


    # Enter a parse tree produced by ExprParser#identifier.
    def enterIdentifier(self, ctx:ExprParser.IdentifierContext):
        pass

    # Exit a parse tree produced by ExprParser#identifier.
    def exitIdentifier(self, ctx:ExprParser.IdentifierContext):
        pass


    # Enter a parse tree produced by ExprParser#parens.
    def enterParens(self, ctx:ExprParser.ParensContext):
        pass

    # Exit a parse tree produced by ExprParser#parens.
    def exitParens(self, ctx:ExprParser.ParensContext):
        pass


    # Enter a parse tree produced by ExprParser#comparison.
    def enterComparison(self, ctx:ExprParser.ComparisonContext):
        pass

    # Exit a parse tree produced by ExprParser#comparison.
    def exitComparison(self, ctx:ExprParser.ComparisonContext):
        pass


    # Enter a parse tree produced by ExprParser#bool.
    def enterBool(self, ctx:ExprParser.BoolContext):
        pass

    # Exit a parse tree produced by ExprParser#bool.
    def exitBool(self, ctx:ExprParser.BoolContext):
        pass


    # Enter a parse tree produced by ExprParser#string.
    def enterString(self, ctx:ExprParser.StringContext):
        pass

    # Exit a parse tree produced by ExprParser#string.
    def exitString(self, ctx:ExprParser.StringContext):
        pass


    # Enter a parse tree produced by ExprParser#assignment.
    def enterAssignment(self, ctx:ExprParser.AssignmentContext):
        pass

    # Exit a parse tree produced by ExprParser#assignment.
    def exitAssignment(self, ctx:ExprParser.AssignmentContext):
        pass


    # Enter a parse tree produced by ExprParser#logicalAnd.
    def enterLogicalAnd(self, ctx:ExprParser.LogicalAndContext):
        pass

    # Exit a parse tree produced by ExprParser#logicalAnd.
    def exitLogicalAnd(self, ctx:ExprParser.LogicalAndContext):
        pass


    # Enter a parse tree produced by ExprParser#float.
    def enterFloat(self, ctx:ExprParser.FloatContext):
        pass

    # Exit a parse tree produced by ExprParser#float.
    def exitFloat(self, ctx:ExprParser.FloatContext):
        pass


    # Enter a parse tree produced by ExprParser#ternaryOperator.
    def enterTernaryOperator(self, ctx:ExprParser.TernaryOperatorContext):
        pass

    # Exit a parse tree produced by ExprParser#ternaryOperator.
    def exitTernaryOperator(self, ctx:ExprParser.TernaryOperatorContext):
        pass


    # Enter a parse tree produced by ExprParser#int.
    def enterInt(self, ctx:ExprParser.IntContext):
        pass

    # Exit a parse tree produced by ExprParser#int.
    def exitInt(self, ctx:ExprParser.IntContext):
        pass


    # Enter a parse tree produced by ExprParser#addSubCon.
    def enterAddSubCon(self, ctx:ExprParser.AddSubConContext):
        pass

    # Exit a parse tree produced by ExprParser#addSubCon.
    def exitAddSubCon(self, ctx:ExprParser.AddSubConContext):
        pass


    # Enter a parse tree produced by ExprParser#unaryMinus.
    def enterUnaryMinus(self, ctx:ExprParser.UnaryMinusContext):
        pass

    # Exit a parse tree produced by ExprParser#unaryMinus.
    def exitUnaryMinus(self, ctx:ExprParser.UnaryMinusContext):
        pass


    # Enter a parse tree produced by ExprParser#relational.
    def enterRelational(self, ctx:ExprParser.RelationalContext):
        pass

    # Exit a parse tree produced by ExprParser#relational.
    def exitRelational(self, ctx:ExprParser.RelationalContext):
        pass


    # Enter a parse tree produced by ExprParser#logicalOr.
    def enterLogicalOr(self, ctx:ExprParser.LogicalOrContext):
        pass

    # Exit a parse tree produced by ExprParser#logicalOr.
    def exitLogicalOr(self, ctx:ExprParser.LogicalOrContext):
        pass


    # Enter a parse tree produced by ExprParser#logicNot.
    def enterLogicNot(self, ctx:ExprParser.LogicNotContext):
        pass

    # Exit a parse tree produced by ExprParser#logicNot.
    def exitLogicNot(self, ctx:ExprParser.LogicNotContext):
        pass


    # Enter a parse tree produced by ExprParser#primitiveType.
    def enterPrimitiveType(self, ctx:ExprParser.PrimitiveTypeContext):
        pass

    # Exit a parse tree produced by ExprParser#primitiveType.
    def exitPrimitiveType(self, ctx:ExprParser.PrimitiveTypeContext):
        pass



del ExprParser