# Project 2

Tento projekt slouží jako ukázka implementace kontinuální integrace v Gitlab.

Je založený na projektu do předmětu PJP a jedná se o implementaci parseru pro jazyk za pomoci ANTLR. Dále obsahuje hledání syntaktických errorů, type checking errorů a VirtualMachine, která vygenerované stack-based instrukce provede.


Pro spuštění můžete použít nix shell:

`nix-shell`

`python driver.py <file>`
    - file je textový soubor s inputem. Bez specifikování file se zpracuje defaultní testový input.